// ==UserScript==
// @name         unanet-search
// @namespace    https://www.unanet.com
// @version      0.1
// @description  Allows you to do a proper search in the project list.
// @author       You
// @match        https://centrelawgroup.unanet.biz/centrelawgroup/action/time/edit?timesheetkey=*
// @grant        none
// @require      https://code.jquery.com/jquery-3.3.1.slim.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js
// @run-at       document-end
// ==/UserScript==

var addRowHolder = addRow;

function newRowsAdded() {
    addRowHolder(document.time.addRowNum);
    instance_select2();
}

function instance_select2() {
    $('.project select').not('.select2-hidden-accessible').select2({
        placeholder: 'Project name',
        width: '100%'
    });

    $('.task select').not('.select2-hidden-accessible').select2({
        placeholder: 'Task name',
        width: '100%',
    });

    $('.project-type select').not('.select2-hidden-accessible').select2({
        placeholder: 'Project type',
        width: '100%',
        dropdownAutoWidth: true
    });
}

function loaded() {
    var css = document.createElement("link");
    css.type = "text/css";
    css.rel  = "stylesheet";
    css.href = 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css'
    document.head.appendChild(css);

    instance_select2();
}

(function() {
    'use strict';
    window.addRow = newRowsAdded;
    window.addEventListener('load', loaded);
})();