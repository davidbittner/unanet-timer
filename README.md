# Unanet Extension Scripts

### Unanet Timer

This is a script for starting a timer with Ctrl+Click on the Unanet timesheet page.

### Unanet Mobile Timer

This is a script for starting a timer with Ctrl+Click on the Unanet Mobile timesheet page.

### Unanet Search

This is a script for changing the Project List into a different dropdown that allows searches more akin to Google.

# Installation Instructions

This script requires a browser extension called Tampermonkey to work. To download this extension, you must first follow the URL corresponding to what browser you are using.

+ [Firefox](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/)
+ [Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en)
+ [Microsoft Edge](https://www.microsoft.com/en-us/p/tampermonkey/9nblggh5162s?cid=msft_web_collection)
+ Internet Explorer is unfortunately not supported by Tampermonkey

Once you go to the link for your browser type, click the install button to add the extension.

Finally, you can go ahead and click the following link to install whichever script you would like:
+ [Unanet Timer](https://gitlab.com/davidbittner/unanet-timer/raw/master/unanet-timer.user.js)
+ [Unanet Mobile Timer](https://gitlab.com/davidbittner/unanet-timer/raw/master/unanet-timer-mobile.user.js)
+ [Unanet Search](https://gitlab.com/davidbittner/unanet-timer/raw/master/unanet-search.user.js)

Once you go to the link, a new tab should open from Tampermonkey. It will have an install button to the left of the screen. Go ahead and click that. A blank tab may open, go ahead and close it. 

You're done! The script should now work.

# Usage

### Unanet Timer and Unanet Mobile Timer

You can now go to your Unanet Timesheet and Ctrl+Click any cell to start a timer.

While the cell is a light shade of green, the timer is counting; The cell will give you a live update of how much time is accumulating.

Once you would like the timer to stop, Ctrl+Click the cell again and it should turn back to the normal shade of color. Once you leave the cell with your cursor, Unanet will round the value to the closest increment of time.

### Unanet Search

You can now go to your Unanet Timesheet and search through the available projects. It works in the way a much more traditional search engine works in the sense that it does not require a full absolute match.
