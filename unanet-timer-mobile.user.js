// ==UserScript==
// @name         unanet-timer-mobile
// @namespace    https://www.unanet.com
// @version      0.1
// @description  Allows you to initiate a timer on the mobile unanet timesheet
// @author       David Bittner
// @match        https://centrelawgroup.unanet.biz/centrelawgroup/action/mobile/timesheet?timesheetkey=*
// @grant        none
// @require      https://code.jquery.com/jquery-3.3.1.slim.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js
// @run-at       document-end
// ==/UserScript==

var ctrl = false;
function set_time(box) {
	let start = box.start;

	let end = new Date().getTime();		
	let hours = (((end-start)/10)/60)/60;

	hours = Math.round(hours*100)/100;

	box.value = box.holder + hours;
}

function box_clicked(box) {
    if(ctrl) {
        if(box.timing == null) {
            box.timing = false;
        }
        box.timing = !box.timing;

        if(box.timing) {
			box.start  = new Date().getTime();
        	box.holder = parseFloat(box.value) || 0;

			box.style.background = 'rgb(212, 237, 203)';
			box.thread_id        = window.setInterval(function() {
				set_time(box);
			}, 200); 

			set_time(box);
		}else{
			clearInterval(box.thread_id);
			set_time(box);

			box.style.background = '';
		}
    }
}

function loaded() {
    $(document).keydown(function(event){
    if(event.which == "17") {
            ctrl = true;
        }
    });
    
    $(document).keyup(function(event){
        if(event.which == "17") {
            ctrl = false;
        }
    });

    $(".hours input").on("click", function() {
        box_clicked(this);
    });
}

(function() {
    'use strict';
    window.addEventListener('load', loaded);
})();
