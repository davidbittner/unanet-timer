// ==UserScript==
// @name         unanet-timer
// @namespace    https://www.unanet.com
// @version      0.1
// @description  Ctrl+Click to start timers in cells, Ctrl+Click to stop.
// @author       David Bittner
// @match        https://centrelawgroup.unanet.biz/centrelawgroup/action/time/edit?timesheetkey=*
// @grant        none
// @run-at       document-end
// ==/UserScript==

function hasClass(element, class_name) {
    class_name = " "+class_name+" ";
    return (" " + element.className + " ").replace(/[\n\t]/g, " ").indexOf(class_name) > -1;
}

function set_time(target) {
    let start = target.start;

    let end = new Date().getTime();
    let hours = (((end - start)/1000)/60)/60;

    target.value = Math.round((target.holder+hours)*100)/100;

    //this is declared in another JS file
    saveRequired = true;
}

function clicked(event) {
    event = event || window.event;

    if(!event.ctrlKey) {
        return;
    }

    let target = event.target || event.srcElement;

    if(hasClass(target, "hours")) {
        if(target.timing == null) {
            target.timing = false;
        }

        target.timing = !target.timing;

        if(target.timing) {
            target.start = new Date().getTime();
            target.holder = parseFloat(target.value) || 0.0;

            target.style.background = 'rgb(212, 237, 203)';

            target.thread_id = window.setInterval(function(){set_time(target);}, 1000);
        }else{
            clearInterval(target.thread_id);
            set_time(target);

            target.style.background = '';
        }
    }
}

(function() {
    'use strict';

    document.addEventListener('click', clicked, false);
})();
